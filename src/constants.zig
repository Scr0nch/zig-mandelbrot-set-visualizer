pub const initial_mandelbrot_set_translation_x: f64 = -0.5;
pub const initial_mandelbrot_set_translation_y: f64 = 0;
pub const initial_mandelbrot_set_scale: f64 = 2;
