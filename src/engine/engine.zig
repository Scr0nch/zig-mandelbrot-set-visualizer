const c = @import("../c.zig").c;
const std = @import("std");
const success = @import("vulkan/success.zig");
const vulkan_data = @import("vulkan/vulkan_data.zig");
const VulkanData = vulkan_data.VulkanData;
const constants = @import("vulkan/constants.zig");
const buffer = @import("vulkan/buffer.zig");
const Buffer = buffer.Buffer;
const matrix = @import("math/matrix.zig");
const UserFunctions = @import("user_functions.zig").UserFunctions;
const swapchain = @import("vulkan/swapchain.zig");

var global_engine_count: usize = 0;

pub fn Engine(comptime UserState: type, comptime Uniforms: type, comptime user_functions: UserFunctions(Uniforms)) type {
    return struct {
        const Self = @This();

        allocator: std.mem.Allocator,
        window: *c.GLFWwindow,
        vulkan_data: VulkanData,
        user_state: UserState,
        uniforms: Uniforms,
        target_fps: u32,

        pub fn init(allocator: std.mem.Allocator, user_state: UserState, uniforms: Uniforms, initial_window_size: i32, target_fps: u32) !Self {
            if (global_engine_count == 0) {
                global_engine_count += 1;
                // initialize GLFW if it has not been already
                try initializeGLFW();
            }
            const window: *c.GLFWwindow = try createWindow(initial_window_size);

            return Self{
                .allocator = allocator,
                .window = window,
                .vulkan_data = try vulkan_data.create(Uniforms, allocator, window),
                .user_state = user_state,
                .uniforms = uniforms,
                .target_fps = target_fps,
            };
        }

        pub fn loop(self: *Self) !void {
            const fps_float: f64 = @floatFromInt(self.target_fps);
            const target_fps_frame_time: u32 = compute_target_fps_frame_time: {
                const fps_microseconds: u32 = @intFromFloat(1.0 / fps_float);
                break :compute_target_fps_frame_time fps_microseconds * 1000;
            };
            var current_frame: usize = 0;

            var is_framebuffer_resized: bool = false;
            // when the window framebuffer size change callback is called it will modify the window's user pointer to notify the
            // main loop that the framebuffer's size has changed
            c.glfwSetWindowUserPointer(self.window, &is_framebuffer_resized);

            var last_time: i64 = std.time.milliTimestamp();
            while (c.glfwWindowShouldClose(self.window) == 0) {
                c.glfwPollEvents();
                const start_time = std.time.milliTimestamp();
                if ((try drawFrame(Uniforms, &current_frame, self.vulkan_data, &self.uniforms)) or is_framebuffer_resized) {
                    // the framebuffer has been resized and/or the swapchain is invalid
                    const extent: c.VkExtent2D = swapchain.pickSwapchainExtent(self.window, try self.vulkan_data.swapchain_support_details.updateAndGetPhysicalDeviceCapabilities());
                    self.uniforms.width = @floatFromInt(extent.width);
                    self.uniforms.height = @floatFromInt(extent.height);

                    try vulkan_data.recreateSwapchain(Uniforms, self.allocator, self.window, &self.vulkan_data);
                    is_framebuffer_resized = false;
                }
                const end_time = std.time.milliTimestamp();
                const frame_time = end_time - start_time;
                const sleep_time = target_fps_frame_time - frame_time;
                if (sleep_time > 0) {
                    const sleep_time_milliseconds_int: u64 = @intCast(sleep_time);
                    std.time.sleep(sleep_time_milliseconds_int * 1000000);
                }

                user_functions.update(self.window, &self.uniforms, @intCast(end_time - last_time));
                last_time = end_time;
            }
        }

        pub fn deinit(self: *Self) void {
            vulkan_data.destroy(self.allocator, &self.vulkan_data);
            c.glfwDestroyWindow(self.window);

            global_engine_count -= 1;
            if (global_engine_count == 0) {
                // terminate GLFW if there are no remaining engines left
                c.glfwTerminate();
            }
        }
    };
}

fn initializeGLFW() !void {
    _ = c.glfwSetErrorCallback(glfwErrorCallback);

    if (c.glfwInit() == 0) {
        return error.GlfwInitFailed;
    }

    if (c.glfwVulkanSupported() == 0) {
        return error.GlfwVulkanNotSupported;
    }
}

fn glfwErrorCallback(error_code: c_int, message: [*c]const u8) callconv(.C) void {
    std.log.warn("GLFW Error {}: \"{s}\"\n", .{ error_code, message });
}

pub fn createWindow(initial_window_size: i32) !*c.GLFWwindow {
    c.glfwWindowHint(c.GLFW_CLIENT_API, c.GLFW_NO_API);
    // do not attempt to steal focus during window creation and create an error on Wayland
    c.glfwWindowHint(c.GLFW_FOCUSED, c.GLFW_FALSE);

    const window: *c.GLFWwindow = c.glfwCreateWindow(initial_window_size, initial_window_size, "Mandelbrot Set Visualizer", null, null) orelse return error.GlfwCreateWindowFailed;

    // let GLFW handle centering, hiding, and locking the mouse
    //c.glfwSetInputMode(engine.window, c.GLFW_CURSOR, c.GLFW_CURSOR_DISABLED);

    _ = c.glfwSetFramebufferSizeCallback(window, glfwSetFramebufferSizeCallback);

    return window;
}

fn glfwSetFramebufferSizeCallback(window_ptr: ?*c.GLFWwindow, width: c_int, height: c_int) callconv(.C) void {
    _ = width;
    _ = height;

    const is_framebuffer_resized_ptr: *bool = @ptrCast(c.glfwGetWindowUserPointer(window_ptr.?));
    is_framebuffer_resized_ptr.* = true;
}

//fn updateUniforms(uniforms: *Uniforms, time: i64, swapchain_aspect_ratio: f32, user_state: *GameState) void {
//    uniforms.mvp_matrix = matrix.getProjectionMatrix(90, swapchain_aspect_ratio, 0, 10);
//
//    uniforms.mvp_matrix = matrix.multiply(matrix.getXRotationMatrix(user_state.camera_roll), uniforms.mvp_matrix);
//    uniforms.mvp_matrix = matrix.multiply(matrix.getYRotationMatrix(user_state.camera_pitch), uniforms.mvp_matrix);
//
//    uniforms.mvp_matrix = matrix.multiply(matrix.createTranslationMatrix(user_state.camera_position), uniforms.mvp_matrix);
//
//    //uniforms.mvp_matrix = matrix.multiply(matrix.getZRotationMatrix(@intToFloat(f32, time) * 3.1415926535 / 2.0 * animation_speed), uniforms.mvp_matrix);
//}

fn drawFrame(comptime Uniforms: type, current_frame: *usize, vk_data: VulkanData, uniforms: *Uniforms) !bool {
    // ensure that a frame is not already being rendered using the current_frame's resources
    try success.require(c.vkWaitForFences(vk_data.logical_device, 1, &vk_data.in_flight_fences[current_frame.*], c.VK_TRUE, std.math.maxInt(u64)));

    var image_index: u32 = undefined;
    {
        const result: c.VkResult = c.vkAcquireNextImageKHR(vk_data.logical_device, vk_data.swapchain, 100000000, vk_data.image_available_semaphores[current_frame.*], null, &image_index);
        if (result == c.VK_ERROR_OUT_OF_DATE_KHR) {
            return true;
        } else if (result != c.VK_SUCCESS and result != c.VK_SUBOPTIMAL_KHR) {
            return error.AcquireNextImageKHRFailed;
        }
    }

    // reset the fence to continue with this frame after the image has been or was failed to be acquired
    // if the image fails to be acquired, the fence should not be reset, otherwise a deadlock will occur
    try success.require(c.vkResetFences(vk_data.logical_device, 1, &vk_data.in_flight_fences[current_frame.*]));

    var wait_semaphores = [_]c.VkSemaphore{vk_data.image_available_semaphores[current_frame.*]};
    var wait_stages = [_]c.VkPipelineStageFlags{c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    const signal_semaphores = [_]c.VkSemaphore{vk_data.render_finished_semaphores[current_frame.*]};

    try buffer.updateUniformBuffer(Uniforms, vk_data.logical_device, vk_data.uniform_buffers[image_index], uniforms);
    try swapchain.transitionImageLayout(vk_data.swapchain_images[image_index], c.VK_IMAGE_LAYOUT_UNDEFINED, c.VK_IMAGE_LAYOUT_GENERAL, c.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, c.VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT, vk_data.logical_device, vk_data.command_pool, vk_data.compute_queue);

    var submit_info = [_]c.VkSubmitInfo{c.VkSubmitInfo{
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &wait_semaphores,
        .pWaitDstStageMask = &wait_stages,
        .commandBufferCount = 1,
        .pCommandBuffers = vk_data.command_buffers.ptr + image_index,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &signal_semaphores,

        .pNext = null,
    }};

    try success.require(c.vkQueueSubmit(vk_data.graphics_queue, 1, &submit_info, vk_data.in_flight_fences[current_frame.*]));

    const swap_chains = [_]c.VkSwapchainKHR{vk_data.swapchain};
    const present_info = c.VkPresentInfoKHR{
        .sType = c.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,

        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &signal_semaphores,

        .swapchainCount = 1,
        .pSwapchains = &swap_chains,

        .pImageIndices = @as(*[1]u32, &image_index),

        .pNext = null,
        .pResults = null,
    };

    {
        const result = c.vkQueuePresentKHR(vk_data.present_queue, &present_info);
        if (result == c.VK_ERROR_OUT_OF_DATE_KHR or result == c.VK_SUBOPTIMAL_KHR) {
            return true;
        } else if (result != c.VK_SUCCESS) {
            return error.QueuePresentationFailed;
        }
    }

    current_frame.* = (current_frame.* + 1) % constants.max_frames_in_flight;

    return false;
}
