const c = @import("../c.zig").c;
const Engine = @import("engine.zig").Engine;

pub fn isKeyPressed(window: *c.GLFWwindow, key: c_int) bool {
    return c.glfwGetKey(window, key) == c.GLFW_PRESS;
}

var isFirst = true;
var last_position: [2]f64 = [2]f64{ 0, 0 };

pub fn getCursorPosition(engine: *Engine) [2]f64 {
    var x_position: f64 = 0;
    var y_position: f64 = 0;
    c.glfwGetCursorPos(engine.window, &x_position, &y_position);
    const next_position = [2]f64{ x_position - last_position[0], y_position - last_position[1] };
    last_position[0] = next_position[0];
    last_position[1] = next_position[1];
    return next_position;
}
