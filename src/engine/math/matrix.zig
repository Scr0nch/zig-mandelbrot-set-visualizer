const std = @import("std");

pub const identity_matrix: [4][4]f32 = [4][4]f32{
    [4]f32{ 1, 0, 0, 0 },
    [4]f32{ 0, 1, 0, 0 },
    [4]f32{ 0, 0, 1, 0 },
    [4]f32{ 0, 0, 0, 1 },
};

pub fn multiply(first: [4][4]f32, second: [4][4]f32) [4][4]f32 {
    var result: [4][4]f32 = undefined;

    var i: usize = 0;
    while (i < 4) : (i += 1) {
        var j: usize = 0;
        while (j < 4) : (j += 1) {
            var k: usize = 0;
            var sum: f32 = 0;
            while (k < 4) : (k += 1) {
                sum += first[i][k] * second[k][j];
            }

            result[i][j] = sum;
        }
    }

    return result;
}

pub fn getProjectionMatrix(fov: f32, aspect_ratio: f32, near: f32, far: f32) [4][4]f32 {
    const scale: f32 = 1 / std.math.tan(fov * 0.5 * 3.1415926535 / 180);

    return [4][4]f32{
        [4]f32{ scale / aspect_ratio, 0, 0, 0 },
        [4]f32{ 0, scale, 0, 0 },
        [4]f32{ 0, 0, far / (far - near), 1 },
        [4]f32{ 0, 0, -far * near / (far - near), 1 },
    };
}

pub fn createTranslationMatrix(translation: [3]f32) [4][4]f32 {
    return [4][4]f32{
        [4]f32{ 1, 0, 0, 0 },
        [4]f32{ 0, 1, 0, 0 },
        [4]f32{ 0, 0, 1, 0 },
        [4]f32{ translation[0], translation[1], translation[2], 1 },
    };
}

pub fn getXRotationMatrix(angle: f32) [4][4]f32 {
    const cosine: f32 = std.math.cos(angle);
    const sine: f32 = std.math.sin(angle);
    return [4][4]f32{
        [4]f32{ 1, 0, 0, 0 },
        [4]f32{ 0, cosine, -sine, 0 },
        [4]f32{ 0, sine, cosine, 0 },
        [4]f32{ 0, 0, 0, 1 },
    };
}

pub fn getYRotationMatrix(angle: f32) [4][4]f32 {
    const cosine: f32 = std.math.cos(angle);
    const sine: f32 = std.math.sin(angle);
    return [4][4]f32{
        [4]f32{ cosine, 0, sine, 0 },
        [4]f32{ 0, 1, 0, 0 },
        [4]f32{ -sine, 0, cosine, 0 },
        [4]f32{ 0, 0, 0, 1 },
    };
}

pub fn getZRotationMatrix(angle: f32) [4][4]f32 {
    const cosine: f32 = std.math.cos(angle);
    const sine: f32 = std.math.sin(angle);
    return [4][4]f32{
        [4]f32{ cosine, -sine, 0, 0 },
        [4]f32{ sine, cosine, 0, 0 },
        [4]f32{ 0, 0, 1, 0 },
        [4]f32{ 0, 0, 0, 1 },
    };
}
