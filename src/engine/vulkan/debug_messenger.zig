const c = @import("../../c.zig").c;
const std = @import("std");
const constants = @import("constants.zig");

usingnamespace @import("constants.zig");

fn debugCallback(message_severity: c.VkDebugUtilsMessageSeverityFlagBitsEXT, obj: u32, callback_data: [*c]const c.VkDebugUtilsMessengerCallbackDataEXT, user_data: ?*anyopaque) callconv(.C) c.VkBool32 {
    _ = obj;
    _ = user_data;
    if (message_severity > c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) {
        std.log.warn("validation layer: {s}\n", .{callback_data.*.pMessage});
    }
    return c.VK_FALSE;
}

pub fn setCreateInfo(create_info: *c.VkDebugUtilsMessengerCreateInfoEXT) void {
    create_info.* = c.VkDebugUtilsMessengerCreateInfoEXT{
        .sType = c.VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .messageSeverity = c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType = c.VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = debugCallback,
        .flags = 0,
        .pUserData = null,
        .pNext = null,
    };
}

fn createDebugUtilsMessengerEXT(instance: c.VkInstance, create_info: *c.VkDebugUtilsMessengerCreateInfoEXT, allocator: ?*c.VkAllocationCallbacks, debug_messenger: *c.VkDebugUtilsMessengerEXT) c.VkResult {
    const pfnVkCreateDebugUtilsMessengerEXTOptional: c.PFN_vkCreateDebugUtilsMessengerEXT = @as(c.PFN_vkCreateDebugUtilsMessengerEXT, @ptrCast(c.vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"))) orelse return c.VK_ERROR_EXTENSION_NOT_PRESENT;
    if (pfnVkCreateDebugUtilsMessengerEXTOptional) |pfnVkCreateDebugUtilsMessengerEXT| {
        return pfnVkCreateDebugUtilsMessengerEXT(instance, create_info, allocator, debug_messenger);
    }
    return c.VK_ERROR_EXTENSION_NOT_PRESENT;
}

fn destroyDebugUtilsMessengerEXT(instance: c.VkInstance, debug_messenger: c.VkDebugUtilsMessengerEXT, allocator: ?*c.VkAllocationCallbacks) void {
    const pfnVkDestroyDebugUtilsMessengerEXTOptional: c.PFN_vkDestroyDebugUtilsMessengerEXT = @as(c.PFN_vkDestroyDebugUtilsMessengerEXT, @ptrCast(c.vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT"))) orelse return;
    if (pfnVkDestroyDebugUtilsMessengerEXTOptional) |pfnVkDestroyDebugUtilsMessengerEXT| {
        pfnVkDestroyDebugUtilsMessengerEXT(instance, debug_messenger, allocator);
    }
}

pub fn create(instance: c.VkInstance, debug_messenger: *c.VkDebugUtilsMessengerEXT) !void {
    if (!constants.enable_validation_layers) return;

    var create_info: c.VkDebugUtilsMessengerCreateInfoEXT = undefined;
    setCreateInfo(&create_info);

    if (createDebugUtilsMessengerEXT(instance, &create_info, null, debug_messenger) != c.VK_SUCCESS) {
        return error.DebugUtilsMessengerCreationFailed;
    }
}

pub fn destroy(instance: c.VkInstance, debug_messenger: c.VkDebugUtilsMessengerEXT) void {
    if (constants.enable_validation_layers) {
        destroyDebugUtilsMessengerEXT(instance, debug_messenger, null);
    }
}
