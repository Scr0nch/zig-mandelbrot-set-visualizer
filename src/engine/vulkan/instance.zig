const c = @import("../../c.zig").c;
const std = @import("std");

const constants = @import("constants.zig");
const success = @import("success.zig");
const debug_messenger = @import("debug_messenger.zig");

fn checkValidationLayerSupport(allocator: std.mem.Allocator) !bool {
    var layerCount: u32 = undefined;

    try success.require(c.vkEnumerateInstanceLayerProperties(&layerCount, null));

    const availableLayers = try allocator.alloc(c.VkLayerProperties, layerCount);
    defer allocator.free(availableLayers);

    try success.require(c.vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.ptr));

    for (constants.validation_layers) |layerName| {
        var layerFound = false;

        for (availableLayers) |layerProperties| {
            if (std.mem.orderZ(u8, layerName, @as([*:0]const u8, @ptrCast(&layerProperties.layerName))) == .eq) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            return false;
        }
    }

    return true;
}

/// caller must free returned memory
fn getRequiredExtensions(allocator: std.mem.Allocator) ![][*]const u8 {
    var glfwExtensionCount: u32 = 0;
    var glfwExtensions: ?[*]const [*]const u8 = @ptrCast(c.glfwGetRequiredInstanceExtensions(&glfwExtensionCount));

    var extensions = std.ArrayList([*]const u8).init(allocator);
    errdefer extensions.deinit();

    try extensions.appendSlice((glfwExtensions.?)[0..glfwExtensionCount]);

    if (constants.enable_validation_layers) {
        try extensions.append(c.VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return extensions.toOwnedSlice();
}

pub fn create(allocator: std.mem.Allocator) !c.VkInstance {
    if (constants.enable_validation_layers and !(try checkValidationLayerSupport(allocator))) {
        return error.ValidationLayerRequestedButNotAvailable;
    }

    const appInfo = comptime c.VkApplicationInfo{
        .sType = c.VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "Hello Triangle",
        .applicationVersion = c.VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "No Engine",
        .engineVersion = c.VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = c.VK_API_VERSION_1_2,
        .pNext = null,
    };

    const extensions = try getRequiredExtensions(allocator);
    defer allocator.free(extensions);

    var debug_create_info: c.VkDebugUtilsMessengerCreateInfoEXT = undefined;
    var p_next: ?*const c.VkDebugUtilsMessengerCreateInfoEXT = undefined;
    if (constants.enable_validation_layers) {
        debug_messenger.setCreateInfo(&debug_create_info);
        p_next = &debug_create_info;
    } else {
        p_next = null;
    }

    const createInfo = c.VkInstanceCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
        .enabledExtensionCount = @intCast(extensions.len),
        .ppEnabledExtensionNames = extensions.ptr,
        .enabledLayerCount = if (constants.enable_validation_layers) constants.validation_layers.len else 0,
        .ppEnabledLayerNames = if (constants.enable_validation_layers) &constants.validation_layers else null,
        .pNext = p_next,
        .flags = 0,
    };

    var instance: c.VkInstance = undefined;
    try success.require(c.vkCreateInstance(&createInfo, null, &instance));

    return instance;
}

pub fn destroy(instance: c.VkInstance) void {
    c.vkDestroyInstance(instance, null);
}
