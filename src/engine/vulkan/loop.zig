const c = @import("../../c.zig").c;
const std = @import("std");
const success = @import("success.zig");
const QueueFamilyIndices = @import("queue_family_indices.zig").QueueFamilyIndices;
const max_frames_in_flight = @import("constants.zig").max_frames_in_flight;
const Buffer = @import("buffer.zig").Buffer;

pub fn createFramebuffers(allocator: std.mem.Allocator, logical_device: c.VkDevice, swapchain_extent: c.VkExtent2D, swapchain_image_views: []c.VkImageView, render_pass: c.VkRenderPass) ![]c.VkFramebuffer {
    var swapchain_framebuffers: []c.VkFramebuffer = try allocator.alloc(c.VkFramebuffer, swapchain_image_views.len);

    for (swapchain_image_views, 0..) |swap_chain_image_view, i| {
        const attachments = [_]c.VkImageView{swap_chain_image_view};

        const framebuffer_info = c.VkFramebufferCreateInfo{
            .sType = c.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = render_pass,
            .attachmentCount = 1,
            .pAttachments = &attachments,
            .width = swapchain_extent.width,
            .height = swapchain_extent.height,
            .layers = 1,

            .pNext = null,
            .flags = 0,
        };

        try success.require(c.vkCreateFramebuffer(logical_device, &framebuffer_info, null, &swapchain_framebuffers[i]));
    }

    return swapchain_framebuffers;
}

pub fn createPool(queue_family_indices: QueueFamilyIndices, logical_device: c.VkDevice) !c.VkCommandPool {
    const pool_info = c.VkCommandPoolCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = queue_family_indices.compute_family.?,

        .pNext = null,
        .flags = 0,
    };

    var command_pool: c.VkCommandPool = undefined;

    try success.require(c.vkCreateCommandPool(logical_device, &pool_info, null, &command_pool));

    return command_pool;
}

pub fn createCommandBuffers(allocator: std.mem.Allocator, logical_device: c.VkDevice, swapchain_extent: c.VkExtent2D, render_pass: c.VkRenderPass, compute_pipeline_layout: c.VkPipelineLayout, compute_pipeline: c.VkPipeline, swapchain_framebuffers: []c.VkFramebuffer, command_pool: c.VkCommandPool, compute_descriptor_sets: []c.VkDescriptorSet) ![]c.VkCommandBuffer {
    const command_buffers: []c.VkCommandBuffer = try allocator.alloc(c.VkCommandBuffer, swapchain_framebuffers.len);

    const command_buffer_alloc_info = c.VkCommandBufferAllocateInfo{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = @intCast(command_buffers.len),
        .pNext = null,
    };

    try success.require(c.vkAllocateCommandBuffers(logical_device, &command_buffer_alloc_info, command_buffers.ptr));

    for (command_buffers, 0..) |command_buffer, i| {
        const begin_info = c.VkCommandBufferBeginInfo{
            .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = c.VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
            .pNext = null,
            .pInheritanceInfo = null,
        };

        try success.require(c.vkBeginCommandBuffer(command_buffer, &begin_info));

        const render_pass_info = c.VkRenderPassBeginInfo{
            .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = render_pass,
            .framebuffer = swapchain_framebuffers[i],
            .renderArea = c.VkRect2D{
                .offset = c.VkOffset2D{ .x = 0, .y = 0 },
                .extent = swapchain_extent,
            },
            .clearValueCount = 0,
            .pClearValues = null,

            .pNext = null,
        };

        c.vkCmdBindPipeline(command_buffer, c.VK_PIPELINE_BIND_POINT_COMPUTE, compute_pipeline);
        c.vkCmdBindDescriptorSets(command_buffer, c.VK_PIPELINE_BIND_POINT_COMPUTE, compute_pipeline_layout, 0, 1, &compute_descriptor_sets[i], 0, null);
        const workgroup_size: f32 = 32;
        // TODO dont exceed VkPhysicalDeviceLimits::maxComputeWorkGroupCount[0, 1]
        const exact_workgroup_width: f32 = @as(f32, @floatFromInt(swapchain_extent.width)) / workgroup_size;
        const exact_workgroup_height: f32 = @as(f32, @floatFromInt(swapchain_extent.height)) / workgroup_size;
        var workgroup_width: u32 = @intFromFloat(exact_workgroup_width);
        if (@as(f32, @floatFromInt(@as(u64, @intFromFloat(exact_workgroup_width)))) != exact_workgroup_width) {
            workgroup_width += 1;
        }
        var workgroup_height: u32 = @intFromFloat(exact_workgroup_height);
        if (@as(f32, @floatFromInt(@as(u32, @intFromFloat(exact_workgroup_height)))) != exact_workgroup_height) {
            workgroup_height += 1;
        }
        c.vkCmdDispatch(command_buffer, workgroup_width, workgroup_height, 1);

        c.vkCmdBeginRenderPass(command_buffer, &render_pass_info, c.VK_SUBPASS_CONTENTS_INLINE);

        //c.vkCmdBindPipeline(command_buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline);
        //c.vkCmdBindDescriptorSets(command_buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline_layout, 0, 1, &uniforms_descriptor_sets[i], 0, null);

        //const vertex_buffers: [1]c.VkBuffer = .{ vertex_buffer.vulkan_buffer };
        //const offsets: [1]c.VkDeviceSize = .{ 0 };
        //c.vkCmdBindVertexBuffers(command_buffer, 0, 1, &vertex_buffers, &offsets);

        //c.vkCmdDraw(command_buffer, 6, 0, 0, 0);

        c.vkCmdEndRenderPass(command_buffer);

        try success.require(c.vkEndCommandBuffer(command_buffer));
    }

    return command_buffers;
}

pub fn createSyncronizationObjects(logical_device: c.VkDevice, image_available_semaphores: []c.VkSemaphore, render_finished_semaphores: []c.VkSemaphore, in_flight_fences: []c.VkFence) !void {
    const semaphore_info = c.VkSemaphoreCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = null,
        .flags = 0,
    };

    const fence_info = c.VkFenceCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = c.VK_FENCE_CREATE_SIGNALED_BIT,
        .pNext = null,
    };

    var i: usize = 0;
    while (i < max_frames_in_flight) : (i += 1) {
        try success.require(c.vkCreateSemaphore(logical_device, &semaphore_info, null, &image_available_semaphores[i]));
        try success.require(c.vkCreateSemaphore(logical_device, &semaphore_info, null, &render_finished_semaphores[i]));
        try success.require(c.vkCreateFence(logical_device, &fence_info, null, &in_flight_fences[i]));
    }
}

pub fn destroyFramebuffers(allocator: std.mem.Allocator, logical_device: c.VkDevice, swapchain_framebuffers: []c.VkFramebuffer) void {
    for (swapchain_framebuffers) |framebuffer| {
        c.vkDestroyFramebuffer(logical_device, framebuffer, null);
    }
    allocator.free(swapchain_framebuffers);
}

pub fn destroyPool(logical_device: c.VkDevice, command_pool: c.VkCommandPool) void {
    c.vkDestroyCommandPool(logical_device, command_pool, null);
}

pub fn destroyCommandBuffers(allocator: std.mem.Allocator, logical_device: c.VkDevice, command_pool: c.VkCommandPool, command_buffers: []c.VkCommandBuffer) void {
    c.vkFreeCommandBuffers(logical_device, command_pool, @intCast(command_buffers.len), command_buffers.ptr);
    allocator.free(command_buffers);
}

pub fn destroySyncronizationObjects(logical_device: c.VkDevice, image_available_semaphores: []c.VkSemaphore, render_finished_semaphores: []c.VkSemaphore, in_flight_fences: []c.VkFence) void {
    var i: usize = 0;
    while (i < max_frames_in_flight) : (i += 1) {
        c.vkDestroySemaphore(logical_device, image_available_semaphores[i], null);
        c.vkDestroySemaphore(logical_device, render_finished_semaphores[i], null);
        c.vkDestroyFence(logical_device, in_flight_fences[i], null);
    }
}
