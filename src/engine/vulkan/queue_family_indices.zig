const std = @import("std");
const c = @import("../../c.zig").c;
const success = @import("success.zig");

pub const QueueFamilyIndices = struct {
    const Self = @This();

    compute_family: ?u32,
    graphics_family: ?u32,
    present_family: ?u32,

    fn init() Self {
        return Self{
            .compute_family = null,
            .graphics_family = null,
            .present_family = null,
        };
    }

    pub fn isComplete(self: *Self) bool {
        return self.compute_family != null and self.graphics_family != null and self.present_family != null;
    }
};

pub fn get(allocator: std.mem.Allocator, surface: c.VkSurfaceKHR, physical_device: c.VkPhysicalDevice) !QueueFamilyIndices {
    var indices: QueueFamilyIndices = QueueFamilyIndices.init();
    // get queue families properties from physical device; it is used to determine which queue families can be used
    // at all and have graphics support
    var queue_families_count: u32 = 0;
    c.vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_families_count, null);
    const queue_families_properties = try allocator.alloc(c.VkQueueFamilyProperties, queue_families_count);
    defer allocator.free(queue_families_properties);
    c.vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_families_count, queue_families_properties.ptr);

    for (queue_families_properties, 0..) |queue_family_properties, i| {
        if (queue_family_properties.queueCount <= 0) {
            continue;
        }

        // check if this queue family can support graphics and presentation; add it to the indices if it is empty
        // to promote splitting work into multiple queues if possible
        // TODO is splitting work into multiple queues better?
        if (queue_family_properties.queueFlags & @as(u32, c.VK_QUEUE_COMPUTE_BIT) != 0 and indices.compute_family == null) {
            indices.compute_family = @intCast(i);
        }

        if (queue_family_properties.queueFlags & @as(u32, c.VK_QUEUE_GRAPHICS_BIT) != 0 and indices.graphics_family == null) {
            indices.graphics_family = @intCast(i);
        }

        var present_support: c.VkBool32 = 0;
        try success.require(c.vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, @intCast(i), surface, &present_support));

        if (present_support != 0 and indices.present_family == null) {
            indices.present_family = @intCast(i);
        }

        if (indices.isComplete()) {
            break;
        }
    }

    return indices;
}
