const std = @import("std");
const c = @import("../../c.zig").c;

pub fn require(result: c.VkResult) !void {
    switch (result) {
        c.VK_SUCCESS, c.VK_INCOMPLETE => return,
        c.VK_ERROR_OUT_OF_HOST_MEMORY, c.VK_ERROR_OUT_OF_DEVICE_MEMORY => return error.OutOfMemory,
        c.VK_ERROR_INITIALIZATION_FAILED => return error.InitializationFailed,
        c.VK_ERROR_UNKNOWN => return error.Unknown,
        else => {
            std.debug.print("Unknown error: {}", .{result});
            return error.Unknown;
        },
    }
}
