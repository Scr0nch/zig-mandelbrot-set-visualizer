const c = @import("../../c.zig").c;
const success = @import("success.zig");

pub fn create(instance: c.VkInstance, window: *c.GLFWwindow) !c.VkSurfaceKHR {
    var surface: c.VkSurfaceKHR = undefined;
    try success.require(c.glfwCreateWindowSurface(instance, window, null, &surface));
    return surface;
}

pub fn destroy(instance: c.VkInstance, surface: c.VkSurfaceKHR) void {
    c.vkDestroySurfaceKHR(instance, surface, null);
}
