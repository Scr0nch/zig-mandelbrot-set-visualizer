const std = @import("std");
const c = @import("../../c.zig").c;
const success = @import("success.zig");

pub const SwapchainSupportDetails = struct {
    const Self = @This();

    physical_device: c.VkPhysicalDevice,
    surface: c.VkSurfaceKHR,
    capabilities: c.VkSurfaceCapabilitiesKHR,
    formats: std.ArrayList(c.VkSurfaceFormatKHR),
    present_modes: std.ArrayList(c.VkPresentModeKHR),

    fn init(allocator: std.mem.Allocator, surface: c.VkSurfaceKHR, physical_device: c.VkPhysicalDevice) Self {
        return Self{
            .physical_device = physical_device,
            .surface = surface,
            .capabilities = undefined,
            .formats = std.ArrayList(c.VkSurfaceFormatKHR).init(allocator),
            .present_modes = std.ArrayList(c.VkPresentModeKHR).init(allocator),
        };
    }

    fn deinit(self: *Self) void {
        self.formats.deinit();
        self.present_modes.deinit();
    }

    pub fn updateAndGetPhysicalDeviceCapabilities(self: *Self) !c.VkSurfaceCapabilitiesKHR {
        try success.require(c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(self.physical_device, self.surface, &self.capabilities));
        return self.capabilities;
    }
};

pub fn create(allocator: std.mem.Allocator, surface: c.VkSurfaceKHR, physical_device: c.VkPhysicalDevice) !SwapchainSupportDetails {
    var details: SwapchainSupportDetails = SwapchainSupportDetails.init(allocator, surface, physical_device);

    try success.require(c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &details.capabilities));

    var format_count: u32 = undefined;
    try success.require(c.vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, null));

    if (format_count != 0) {
        try details.formats.resize(format_count);
        try success.require(c.vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, details.formats.items.ptr));
    }

    var present_mode_count: u32 = undefined;
    try success.require(c.vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_count, null));

    if (present_mode_count != 0) {
        try details.present_modes.resize(present_mode_count);
        try success.require(c.vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_count, details.present_modes.items.ptr));
    }

    return details;
}

pub fn destroy(swap_chain_support_details: *SwapchainSupportDetails) void {
    swap_chain_support_details.deinit();
}
