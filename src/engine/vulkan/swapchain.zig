const std = @import("std");
const c = @import("../../c.zig").c;
const success = @import("success.zig");
const SwapchainSupportDetails = @import("swap_chain_support.zig").SwapchainSupportDetails;
const QueueFamilyIndices = @import("queue_family_indices.zig").QueueFamilyIndices;

fn pickSwapchainSurfaceFormat(available_formats: []c.VkSurfaceFormatKHR) c.VkSurfaceFormatKHR {
    if (available_formats.len == 1 and available_formats[0].format == c.VK_FORMAT_UNDEFINED) {
        return c.VkSurfaceFormatKHR{
            .format = c.VK_FORMAT_B8G8R8A8_UNORM,
            .colorSpace = c.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
        };
    }

    for (available_formats) |available_format| {
        if (available_format.format == c.VK_FORMAT_B8G8R8A8_UNORM and available_format.colorSpace == c.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return available_format;
        }
    }

    return available_formats[0];
}

fn pickSwapchainPresentMode(available_present_modes: []c.VkPresentModeKHR) c.VkPresentModeKHR {
    var best_mode: c.VkPresentModeKHR = c.VK_PRESENT_MODE_FIFO_KHR;

    for (available_present_modes) |available_present_mode| {
        if (available_present_mode == c.VK_PRESENT_MODE_MAILBOX_KHR) {
            return available_present_mode;
        } else if (available_present_mode == c.VK_PRESENT_MODE_IMMEDIATE_KHR) {
            best_mode = available_present_mode;
        }
    }

    return best_mode;
}

pub fn pickSwapchainExtent(window: *c.GLFWwindow, capabilities: c.VkSurfaceCapabilitiesKHR) c.VkExtent2D {
    var width: c_int = undefined;
    var height: c_int = undefined;
    c.glfwGetFramebufferSize(window, &width, &height);

    var actual_extent = c.VkExtent2D{
        .width = @intCast(width),
        .height = @intCast(height),
    };

    actual_extent.width = @max(capabilities.minImageExtent.width, @min(capabilities.maxImageExtent.width, actual_extent.width));
    actual_extent.height = @max(capabilities.minImageExtent.height, @min(capabilities.maxImageExtent.height, actual_extent.height));

    return actual_extent;
}

pub fn create(allocator: std.mem.Allocator, window: *c.GLFWwindow, surface: c.VkSurfaceKHR, logical_device: c.VkDevice, queue_family_indices: QueueFamilyIndices, swap_chain_support_details: *SwapchainSupportDetails, swapchain_image_format: *c.VkFormat, swapchain_extent: *c.VkExtent2D, swapchain_images: *[]c.VkImage) !c.VkSwapchainKHR {
    const surface_format = pickSwapchainSurfaceFormat(swap_chain_support_details.formats.items);
    const present_mode = pickSwapchainPresentMode(swap_chain_support_details.present_modes.items);
    const extent = pickSwapchainExtent(window, try swap_chain_support_details.updateAndGetPhysicalDeviceCapabilities());

    var image_count: u32 = swap_chain_support_details.capabilities.minImageCount + 1;
    if (swap_chain_support_details.capabilities.maxImageCount > 0 and image_count > swap_chain_support_details.capabilities.maxImageCount) {
        image_count = swap_chain_support_details.capabilities.maxImageCount;
    }

    const queue_family_indices_array = [_]u32{ queue_family_indices.compute_family.?, queue_family_indices.present_family.? };
    const queue_families_different = queue_family_indices.compute_family.? != queue_family_indices.present_family.?;

    const create_info = c.VkSwapchainCreateInfoKHR{
        .sType = c.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = surface,

        .minImageCount = image_count,
        .imageFormat = surface_format.format,
        .imageColorSpace = surface_format.colorSpace,
        .imageExtent = extent,
        .imageArrayLayers = 1,
        // allow swapchain images to be used as a color attachment and storage for the graphics and compute shaders respectively
        // FIXME check if the images support storage
        .imageUsage = c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | c.VK_IMAGE_USAGE_STORAGE_BIT,

        .imageSharingMode = if (queue_families_different) c.VK_SHARING_MODE_CONCURRENT else c.VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = if (queue_families_different) 2 else 0,
        .pQueueFamilyIndices = if (queue_families_different) &queue_family_indices_array else &([_]u32{ 0, 0 }),

        .preTransform = swap_chain_support_details.capabilities.currentTransform,
        .compositeAlpha = c.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .clipped = c.VK_TRUE,

        // TODO use this
        .oldSwapchain = null,

        .pNext = null,
        .flags = 0,
    };

    var swapchain: c.VkSwapchainKHR = undefined;

    try success.require(c.vkCreateSwapchainKHR(logical_device, &create_info, null, &swapchain));

    try success.require(c.vkGetSwapchainImagesKHR(logical_device, swapchain, &image_count, null));
    swapchain_images.* = try allocator.alloc(c.VkImage, image_count);
    try success.require(c.vkGetSwapchainImagesKHR(logical_device, swapchain, &image_count, swapchain_images.*.ptr));

    swapchain_image_format.* = surface_format.format;
    swapchain_extent.* = extent;

    return swapchain;
}

pub fn destroy(logical_device: c.VkDevice, swapchain: c.VkSwapchainKHR) void {
    c.vkDestroySwapchainKHR(logical_device, swapchain, null);
}

pub fn createImageViews(allocator: std.mem.Allocator, logical_device: c.VkDevice, swapchain_image_format: c.VkFormat, swapchain_images: []c.VkImage) ![]c.VkImageView {
    var swapchain_image_views: []c.VkImageView = try allocator.alloc(c.VkImageView, swapchain_images.len);
    errdefer allocator.free(swapchain_image_views);

    for (swapchain_images, 0..) |swap_chain_image, i| {
        const create_info = c.VkImageViewCreateInfo{
            .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = swap_chain_image,
            .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
            .format = swapchain_image_format,
            .components = c.VkComponentMapping{
                .r = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = c.VK_COMPONENT_SWIZZLE_IDENTITY,
            },
            .subresourceRange = c.VkImageSubresourceRange{
                .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },

            .pNext = null,
            .flags = 0,
        };

        try success.require(c.vkCreateImageView(logical_device, &create_info, null, &swapchain_image_views[i]));
    }

    return swapchain_image_views;
}

pub fn destroyImageViews(allocator: std.mem.Allocator, logical_device: c.VkDevice, swapchain_image_views: []c.VkImageView) void {
    for (swapchain_image_views) |image_view| {
        c.vkDestroyImageView(logical_device, image_view, null);
    }
    allocator.free(swapchain_image_views);
}

pub fn transitionImageLayout(image: c.VkImage, old_layout: c.VkImageLayout, new_layout: c.VkImageLayout, source_stage: c.VkPipelineStageFlags, destination_stage: c.VkPipelineStageFlags, logical_device: c.VkDevice, command_pool: c.VkCommandPool, compute_queue: c.VkQueue) !void {
    const command_buffer = try beginSingleTimeCommands(logical_device, command_pool);

    const barrier: c.VkImageMemoryBarrier = .{
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = old_layout,
        .newLayout = new_layout,
        .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .srcAccessMask = 0,
        .dstAccessMask = 0,
        .pNext = null,
    };

    c.vkCmdPipelineBarrier(command_buffer, source_stage, destination_stage, 0, 0, null, 0, null, 1, &barrier);

    try endSingleTimeCommands(logical_device, command_pool, compute_queue, command_buffer);
}

fn beginSingleTimeCommands(logical_device: c.VkDevice, command_pool: c.VkCommandPool) !c.VkCommandBuffer {
    const command_buffer_allocation_info: c.VkCommandBufferAllocateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandPool = command_pool,
        .commandBufferCount = 1,
        .pNext = null,
    };

    var command_buffer: c.VkCommandBuffer = undefined;
    try success.require(c.vkAllocateCommandBuffers(logical_device, &command_buffer_allocation_info, &command_buffer));

    const command_buffer_begin_info: c.VkCommandBufferBeginInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = c.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = null,
        .pNext = null,
    };

    try success.require(c.vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info));

    return command_buffer;
}

fn endSingleTimeCommands(logical_device: c.VkDevice, command_pool: c.VkCommandPool, compute_queue: c.VkQueue, command_buffer: c.VkCommandBuffer) !void {
    try success.require(c.vkEndCommandBuffer(command_buffer));

    const submit_info: c.VkSubmitInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &command_buffer,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = null,
        .pWaitDstStageMask = 0,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = null,
        .pNext = null,
    };

    try success.require(c.vkQueueSubmit(compute_queue, 1, &submit_info, null));
    try success.require(c.vkQueueWaitIdle(compute_queue));

    c.vkFreeCommandBuffers(logical_device, command_pool, 1, &command_buffer);
}
