const std = @import("std");
const c = @import("../../c.zig").c;
const success = @import("success.zig");
const Buffer = @import("buffer.zig").Buffer;

pub fn createGraphicsDescriptorSetLayout(logical_device: c.VkDevice) !c.VkDescriptorSetLayout {
    const sampler_layout_binding: c.VkDescriptorSetLayoutBinding = .{
        .binding = 0,
        .descriptorCount = 1,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImmutableSamplers = null,
        .stageFlags = c.VK_SHADER_STAGE_FRAGMENT_BIT,
    };

    const layout_info: c.VkDescriptorSetLayoutCreateInfo = c.VkDescriptorSetLayoutCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = 1,
        .pBindings = &sampler_layout_binding,
        .flags = 0,
        .pNext = null,
    };

    var descriptor_set_layout: c.VkDescriptorSetLayout = undefined;

    try success.require(c.vkCreateDescriptorSetLayout(logical_device, &layout_info, null, &descriptor_set_layout));

    return descriptor_set_layout;
}

pub fn destroyDescriptorSetLayout(logical_device: c.VkDevice, descriptor_set_layout: c.VkDescriptorSetLayout) void {
    c.vkDestroyDescriptorSetLayout(logical_device, descriptor_set_layout, null);
}

pub fn createGraphicsDescriptorPool(logical_device: c.VkDevice, swapchain_image_count: usize) !c.VkDescriptorPool {
    const pool_size: c.VkDescriptorPoolSize = c.VkDescriptorPoolSize{
        .type = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = @intCast(swapchain_image_count),
    };

    const pool_info: c.VkDescriptorPoolCreateInfo = c.VkDescriptorPoolCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .poolSizeCount = 1,
        .pPoolSizes = &pool_size,
        .maxSets = @intCast(swapchain_image_count),
        .flags = 0,
        .pNext = null,
    };

    var descriptor_pool: c.VkDescriptorPool = undefined;

    try success.require(c.vkCreateDescriptorPool(logical_device, &pool_info, null, &descriptor_pool));

    return descriptor_pool;
}

pub fn destroyDescriptorPool(logical_device: c.VkDevice, descriptor_pool: c.VkDescriptorPool) void {
    c.vkDestroyDescriptorPool(logical_device, descriptor_pool, null);
}

/// caller must free descriptor sets
pub fn createGraphicsDescriptorSets(allocator: std.mem.Allocator, logical_device: c.VkDevice, descriptor_set_layout: c.VkDescriptorSetLayout, descriptor_pool: c.VkDescriptorPool, descriptor_set_count: u32, texture_image_view: c.VkImageView, texture_sampler: c.VkSampler) ![]c.VkDescriptorSet {
    var descriptor_set_layouts: []c.VkDescriptorSetLayout = try allocator.alloc(c.VkDescriptorSetLayout, descriptor_set_count);
    defer allocator.free(descriptor_set_layouts);

    var i: usize = 0;
    while (i < descriptor_set_count) : (i += 1) {
        descriptor_set_layouts[i] = descriptor_set_layout;
    }

    const allocation_info: c.VkDescriptorSetAllocateInfo = c.VkDescriptorSetAllocateInfo{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = descriptor_pool,
        .descriptorSetCount = descriptor_set_count,
        .pSetLayouts = descriptor_set_layouts.ptr,
        .pNext = null,
    };

    const descriptor_sets: []c.VkDescriptorSet = try allocator.alloc(c.VkDescriptorSet, descriptor_set_count);

    try success.require(c.vkAllocateDescriptorSets(logical_device, &allocation_info, descriptor_sets.ptr));

    i = 0;
    while (i < descriptor_set_count) : (i += 1) {
        const descriptor_image_info: c.VkDescriptorImageInfo = .{
            .imageLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            .imageView = texture_image_view,
            .sampler = texture_sampler,
        };

        const descriptor_write: c.VkWriteDescriptorSet = c.VkWriteDescriptorSet{
            .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = descriptor_sets[i],
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .pBufferInfo = null,
            .pImageInfo = descriptor_image_info,
            .pTexelBufferView = null,
            .pNext = null,
        };

        c.vkUpdateDescriptorSets(logical_device, 1, &descriptor_write, 0, null);
    }

    return descriptor_sets;
}

// TODO probably put somewhere else
pub fn createComputeDescriptorSetLayout(logical_device: c.VkDevice) !c.VkDescriptorSetLayout {
    const ubo_layout_binding: c.VkDescriptorSetLayoutBinding = .{
        .binding = 0,
        .descriptorCount = 1,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .pImmutableSamplers = null,
        .stageFlags = c.VK_SHADER_STAGE_COMPUTE_BIT,
    };

    const images_layout_binding: c.VkDescriptorSetLayoutBinding = .{
        .binding = 1,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
        .descriptorCount = 1,
        .stageFlags = c.VK_SHADER_STAGE_COMPUTE_BIT,
        .pImmutableSamplers = null,
    };

    const binding_count = 2;
    const descriptor_set_layout_bindings: [binding_count]c.VkDescriptorSetLayoutBinding = .{ ubo_layout_binding, images_layout_binding };

    const descriptor_set_layout_create_info: c.VkDescriptorSetLayoutCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = binding_count,
        .pBindings = &descriptor_set_layout_bindings,
        .flags = 0,
        .pNext = null,
    };

    var descriptor_set_layout: c.VkDescriptorSetLayout = undefined;

    try success.require(c.vkCreateDescriptorSetLayout(logical_device, &descriptor_set_layout_create_info, null, &descriptor_set_layout));

    return descriptor_set_layout;
}

pub fn createComputeDescriptorPool(logical_device: c.VkDevice, swapchain_image_count: usize) !c.VkDescriptorPool {
    const uniforms_descriptor_pool_size: c.VkDescriptorPoolSize = .{
        .type = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = @intCast(swapchain_image_count),
    };

    const swapchain_images_descriptor_pool_size: c.VkDescriptorPoolSize = .{
        .type = c.VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
        .descriptorCount = @intCast(swapchain_image_count),
    };

    const pool_size_count = 2;
    const descriptor_pool_sizes: [pool_size_count]c.VkDescriptorPoolSize = .{ uniforms_descriptor_pool_size, swapchain_images_descriptor_pool_size };

    const descriptor_pool_create_info: c.VkDescriptorPoolCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = @intCast(swapchain_image_count),
        .poolSizeCount = pool_size_count,
        .pPoolSizes = &descriptor_pool_sizes,
        .flags = 0,
        .pNext = null,
    };

    var descriptor_pool: c.VkDescriptorPool = undefined;

    try success.require(c.vkCreateDescriptorPool(logical_device, &descriptor_pool_create_info, null, &descriptor_pool));

    return descriptor_pool;
}

pub fn createComputeDescriptorSets(comptime Uniforms: type, allocator: std.mem.Allocator, logical_device: c.VkDevice, descriptor_set_layout: c.VkDescriptorSetLayout, descriptor_pool: c.VkDescriptorPool, uniform_buffers: []Buffer, swapchain_image_views: []c.VkImageView) ![]c.VkDescriptorSet {
    var descriptor_set_layouts: []c.VkDescriptorSetLayout = try allocator.alloc(c.VkDescriptorSetLayout, swapchain_image_views.len);
    defer allocator.free(descriptor_set_layouts);

    var i: usize = 0;
    while (i < swapchain_image_views.len) : (i += 1) {
        descriptor_set_layouts[i] = descriptor_set_layout;
    }

    const allocation_info: c.VkDescriptorSetAllocateInfo = c.VkDescriptorSetAllocateInfo{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = descriptor_pool,
        .descriptorSetCount = @intCast(swapchain_image_views.len),
        .pSetLayouts = descriptor_set_layouts.ptr,
        .pNext = null,
    };

    const descriptor_sets: []c.VkDescriptorSet = try allocator.alloc(c.VkDescriptorSet, swapchain_image_views.len);

    try success.require(c.vkAllocateDescriptorSets(logical_device, &allocation_info, descriptor_sets.ptr));

    i = 0;
    while (i < swapchain_image_views.len) : (i += 1) {
        const descriptor_uniform_info: c.VkDescriptorBufferInfo = .{
            .buffer = uniform_buffers[i].vulkan_buffer,
            .offset = 0,
            .range = @sizeOf(Uniforms),
        };

        const descriptor_image_info: c.VkDescriptorImageInfo = .{
            .imageLayout = c.VK_IMAGE_LAYOUT_GENERAL,
            .imageView = swapchain_image_views[i],
            .sampler = null,
        };

        const write_descriptor_sets_count = 2;
        const write_descriptor_sets: [write_descriptor_sets_count]c.VkWriteDescriptorSet = .{
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_sets[i],
                .dstArrayElement = 0,
                .dstBinding = 0,
                .descriptorCount = 1,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .pBufferInfo = &descriptor_uniform_info,
                .pImageInfo = null,
                .pTexelBufferView = null,
                .pNext = null,
            },
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_sets[i],
                .dstArrayElement = 0,
                .dstBinding = 1,
                .descriptorCount = 1,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                .pBufferInfo = null,
                .pImageInfo = &descriptor_image_info,
                .pTexelBufferView = null,
                .pNext = null,
            },
        };

        c.vkUpdateDescriptorSets(logical_device, write_descriptor_sets_count, &write_descriptor_sets, 0, null);
    }

    return descriptor_sets;
}
