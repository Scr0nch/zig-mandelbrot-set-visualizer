const c = @import("../../c.zig").c;

pub const Vertex = packed struct {
    position: [2]f32,
    color: [3]f32,
};

pub const vertex_binding_description: c.VkVertexInputBindingDescription = c.VkVertexInputBindingDescription{
    .binding = 0,
    .stride = @sizeOf(Vertex),
    .inputRate = c.VK_VERTEX_INPUT_RATE_VERTEX,
};

pub const vertex_attribute_descriptions: [2]c.VkVertexInputAttributeDescription = [2]c.VkVertexInputAttributeDescription{
    .{
        .binding = 0,
        .location = 0,
        .format = c.VK_FORMAT_R32G32_SFLOAT,
        .offset = @offsetOf(Vertex, "position"),
    },
    .{
        .binding = 0,
        .location = 1,
        .format = c.VK_FORMAT_R32G32B32_SFLOAT,
        .offset = @offsetOf(Vertex, "color"),
    },
};
