const c = @import("../../c.zig").c;
const std = @import("std");
const success = @import("success.zig");

const max_frames_in_flight = @import("constants.zig").max_frames_in_flight;
const instance = @import("instance.zig");
const debug_messenger = @import("debug_messenger.zig");
const surface = @import("surface.zig");
const QueueFamilyIndices = @import("queue_family_indices.zig").QueueFamilyIndices;
const swap_chain_support = @import("swap_chain_support.zig");
const physical_device = @import("physical_device.zig");
const logical_device = @import("logical_device.zig");
const swapchain = @import("swapchain.zig");
const pipeline = @import("pipeline.zig");
const loop = @import("loop.zig");
const buffer = @import("buffer.zig");
const Buffer = buffer.Buffer;
const uniforms = @import("uniforms.zig");

pub const VulkanData = struct { instance: c.VkInstance, debug_messenger: c.VkDebugUtilsMessengerEXT, surface: c.VkSurfaceKHR, physical_device: c.VkPhysicalDevice, swapchain_support_details: swap_chain_support.SwapchainSupportDetails, queue_family_indices: QueueFamilyIndices, logical_device: c.VkDevice, compute_queue: c.VkQueue, graphics_queue: c.VkQueue, present_queue: c.VkQueue, swapchain: c.VkSwapchainKHR, swapchain_image_format: c.VkFormat, swapchain_extent: c.VkExtent2D, swapchain_images: []c.VkImage, swapchain_image_views: []c.VkImageView, render_pass: c.VkRenderPass, compute_pipeline_layout: c.VkPipelineLayout, graphics_pipeline_layout: c.VkPipelineLayout, graphics_descriptor_set_layout: c.VkDescriptorSetLayout, compute_descriptor_set_layout: c.VkDescriptorSetLayout, graphics_pipeline: c.VkPipeline, compute_pipeline: c.VkPipeline, swapchain_framebuffers: []c.VkFramebuffer, command_pool: c.VkCommandPool, vertex_buffer: Buffer, index_buffer: Buffer, command_buffers: []c.VkCommandBuffer, image_available_semaphores: [max_frames_in_flight]c.VkSemaphore, render_finished_semaphores: [max_frames_in_flight]c.VkSemaphore, in_flight_fences: [max_frames_in_flight]c.VkFence, uniform_buffers: []Buffer, compute_buffer: Buffer, graphics_descriptor_pool: c.VkDescriptorPool, graphics_descriptor_sets: []c.VkDescriptorSet, compute_descriptor_pool: c.VkDescriptorPool, compute_descriptor_sets: []c.VkDescriptorSet };

pub fn create(comptime Uniforms: type, allocator: std.mem.Allocator, window: *c.GLFWwindow) !VulkanData {
    // initialize VulkanData struct fields in the memory of the struct instead of copying values into the struct
    // e.g. NOT var _debug_messenger: c.VkDebugUtilsMessengerEXT = debug_messenger.create(...);
    //          return VulkanData { .debug_messenger = _debug_messenger, ... };
    //      YES debug_messenger.create(..., &vk_data.debug_messenger);
    // so that segfaults are not created when the value (_debug_messenger) points to an invalid memory location (e.g. 0x10000000001)
    var vk_data: VulkanData = undefined;

    vk_data.instance = try instance.create(allocator);
    errdefer instance.destroy(vk_data.instance);

    // even though the debug messenger is not depended on by any other object it should be created now so that it
    // will report any errors generated while creating the rest of the vulkan data
    try debug_messenger.create(vk_data.instance, &vk_data.debug_messenger);
    errdefer debug_messenger.destroy(vk_data.instance, vk_data.debug_messenger);

    vk_data.surface = try surface.create(vk_data.instance, window);
    errdefer surface.destroy(vk_data.instance, vk_data.surface);

    // save the queue family indices and swap chain support for later; they will be re-used after their creation during
    // the physical device picking process
    vk_data.physical_device = try physical_device.pick(allocator, vk_data.instance, vk_data.surface, &vk_data.queue_family_indices, &vk_data.swapchain_support_details);
    errdefer swap_chain_support.destroy(&vk_data.swapchain_support_details);

    // create the logical device and its associated queues
    vk_data.logical_device = try logical_device.create(allocator, vk_data.physical_device, vk_data.queue_family_indices, &vk_data.compute_queue, &vk_data.graphics_queue, &vk_data.present_queue);
    errdefer logical_device.destroy(vk_data.logical_device);

    //vk_data.graphics_descriptor_set_layout = try uniforms.createGraphicsDescriptorSetLayout(vk_data.logical_device);
    //errdefer uniforms.destroyDescriptorSetLayout(vk_data.logical_device, vk_data.graphics_descriptor_set_layout);
    vk_data.compute_descriptor_set_layout = try uniforms.createComputeDescriptorSetLayout(vk_data.logical_device);
    errdefer uniforms.destroyDescriptorSetLayout(vk_data.logical_device, vk_data.compute_descriptor_set_layout);

    // create objects that do not depend on the swapchain
    vk_data.command_pool = try loop.createPool(vk_data.queue_family_indices, vk_data.logical_device);
    errdefer loop.destroyPool(vk_data.logical_device, vk_data.command_pool);
    try loop.createSyncronizationObjects(vk_data.logical_device, &vk_data.image_available_semaphores, &vk_data.render_finished_semaphores, &vk_data.in_flight_fences);
    errdefer loop.destroySyncronizationObjects(vk_data.logical_device, &vk_data.image_available_semaphores, &vk_data.render_finished_semaphores, &vk_data.in_flight_fences);

    // create resources
    //vk_data.vertex_buffer = try buffer.createVertexBuffer(vk_data.physical_device, vk_data.logical_device);
    //errdefer buffer.destroyBuffer(vk_data.logical_device, vk_data.vertex_buffer);
    //vk_data.index_buffer = try buffer.createIndexBuffer(vk_data.physical_device, vk_data.logical_device);
    //errdefer buffer.destroyBuffer(vk_data.logical_device, vk_data.index_buffer);

    try createSwapchainAndDependencies(Uniforms, allocator, window, &vk_data);
    errdefer destroySwapchainAndDependencies(allocator, &vk_data);

    return vk_data;
}

fn createSwapchainAndDependencies(comptime Uniforms: type, allocator: std.mem.Allocator, window_ptr: *c.GLFWwindow, vk_data: *VulkanData) !void {
    // create the swapchain and its associated metadata, images, and image views
    vk_data.swapchain = try swapchain.create(allocator, window_ptr, vk_data.surface, vk_data.logical_device, vk_data.queue_family_indices, &vk_data.swapchain_support_details, &vk_data.swapchain_image_format, &vk_data.swapchain_extent, &vk_data.swapchain_images);
    vk_data.swapchain_image_views = try swapchain.createImageViews(allocator, vk_data.logical_device, vk_data.swapchain_image_format, vk_data.swapchain_images);

    // create swapchain dependencies
    vk_data.uniform_buffers = try buffer.createUniformBuffers(Uniforms, allocator, vk_data.physical_device, vk_data.logical_device, vk_data.swapchain_images.len);

    // create the compute pipeline
    vk_data.compute_descriptor_pool = try uniforms.createComputeDescriptorPool(vk_data.logical_device, vk_data.swapchain_images.len);
    vk_data.compute_descriptor_sets = try uniforms.createComputeDescriptorSets(Uniforms, allocator, vk_data.logical_device, vk_data.compute_descriptor_set_layout, vk_data.compute_descriptor_pool, vk_data.uniform_buffers, vk_data.swapchain_image_views);
    vk_data.compute_pipeline = try pipeline.createComputePipeline(vk_data.logical_device, vk_data.compute_descriptor_set_layout, &vk_data.compute_pipeline_layout);

    // create the render pass and graphics pipeline
    vk_data.render_pass = try pipeline.createRenderPass(vk_data.logical_device, vk_data.swapchain_image_format);
    //vk_data.graphics_pipeline = try pipeline.createGraphicsPipeline(allocator, vk_data.logical_device, vk_data.swapchain_extent, vk_data.graphics_descriptor_set_layout, &vk_data.graphics_pipeline_layout, vk_data.render_pass);

    //vk_data.graphics_descriptor_pool = try uniforms.createGraphicsDescriptorPool(vk_data.logical_device, vk_data.swapchain_images.len);
    //vk_data.graphics_descriptor_sets = try uniforms.createGraphicsDescriptorSets(allocator, vk_data.logical_device, vk_data.graphics_descriptor_set_layout, vk_data.uniform_buffers, vk_data.graphics_descriptor_pool);

    // create objects used in the main loop
    vk_data.swapchain_framebuffers = try loop.createFramebuffers(allocator, vk_data.logical_device, vk_data.swapchain_extent, vk_data.swapchain_image_views, vk_data.render_pass);
    vk_data.command_buffers = try loop.createCommandBuffers(allocator, vk_data.logical_device, vk_data.swapchain_extent, vk_data.render_pass, vk_data.compute_pipeline_layout, vk_data.compute_pipeline, vk_data.swapchain_framebuffers, vk_data.command_pool, vk_data.compute_descriptor_sets);
}

pub fn destroy(allocator: std.mem.Allocator, vk_data: *VulkanData) void {
    if (vk_data.logical_device) |vk_data_logical_device| {
        // wait until the device is idle before modifying resources that may be in use; ignore out of memory errors
        _ = c.vkDeviceWaitIdle(vk_data_logical_device);

        destroySwapchainAndDependencies(allocator, vk_data);

        //buffer.destroyBuffer(vk_data.logical_device, vk_data.index_buffer);
        //buffer.destroyBuffer(vk_data.logical_device, vk_data.vertex_buffer);

        loop.destroySyncronizationObjects(vk_data_logical_device, &vk_data.image_available_semaphores, &vk_data.render_finished_semaphores, &vk_data.in_flight_fences);
        loop.destroyPool(vk_data_logical_device, vk_data.command_pool);

        //uniforms.destroyDescriptorSetLayout(vk_data.logical_device, vk_data.graphics_descriptor_set_layout);
        uniforms.destroyDescriptorSetLayout(vk_data.logical_device, vk_data.compute_descriptor_set_layout);

        logical_device.destroy(vk_data_logical_device);
    }

    swap_chain_support.destroy(&vk_data.swapchain_support_details);

    if (vk_data.instance) |vk_data_instance| {
        surface.destroy(vk_data_instance, vk_data.surface);
        debug_messenger.destroy(vk_data_instance, vk_data.debug_messenger);

        instance.destroy(vk_data.instance);
    }
}

fn destroySwapchainAndDependencies(allocator: std.mem.Allocator, vk_data: *VulkanData) void {
    if (vk_data.logical_device) |vk_data_logical_device| {
        allocator.free(vk_data.compute_descriptor_sets);
        //allocator.free(vk_data.graphics_descriptor_sets);
        uniforms.destroyDescriptorPool(vk_data_logical_device, vk_data.compute_descriptor_pool);
        //uniforms.destroyDescriptorPool(vk_data_logical_device, vk_data.graphics_descriptor_pool);
        for (vk_data.uniform_buffers) |uniform_buffer| {
            buffer.destroyBuffer(vk_data.logical_device, uniform_buffer);
        }
        allocator.free(vk_data.uniform_buffers);

        loop.destroyCommandBuffers(allocator, vk_data_logical_device, vk_data.command_pool, vk_data.command_buffers);
        loop.destroyFramebuffers(allocator, vk_data_logical_device, vk_data.swapchain_framebuffers);

        pipeline.destroy(vk_data_logical_device, vk_data.compute_pipeline_layout, vk_data.compute_pipeline);
        //pipeline.destroy(vk_data_logical_device, vk_data.graphics_pipeline_layout, vk_data.graphics_pipeline);
        pipeline.destroyRenderPass(vk_data_logical_device, vk_data.render_pass);

        swapchain.destroyImageViews(allocator, vk_data_logical_device, vk_data.swapchain_image_views);
        allocator.free(vk_data.swapchain_images);
        swapchain.destroy(vk_data_logical_device, vk_data.swapchain);
    }
}

pub fn recreateSwapchain(comptime Uniforms: type, allocator: std.mem.Allocator, window_ptr: *c.GLFWwindow, vk_data_ptr: *VulkanData) !void {
    // if the window is iconified, the swapchain does not need to be recreated until it is uniconified
    var width: c_int = 0;
    var height: c_int = 0;
    c.glfwGetFramebufferSize(window_ptr, &width, &height);
    while (width == 0 or height == 0) {
        c.glfwGetFramebufferSize(window_ptr, &width, &height);
        c.glfwWaitEvents();
    }

    // wait until the device is idle before modifying resources that may be in use; ignore out of memory errors
    _ = c.vkDeviceWaitIdle(vk_data_ptr.logical_device);

    destroySwapchainAndDependencies(allocator, vk_data_ptr);
    try createSwapchainAndDependencies(Uniforms, allocator, window_ptr, vk_data_ptr);
}
