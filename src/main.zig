const std = @import("std");
const Engine = @import("engine/engine.zig").Engine;
const MandelbrotSetState = @import("mandelbrot_set_state.zig");
const Uniforms = @import("uniforms.zig").Uniforms;
const user_functions_impl = @import("user_functions_impl.zig");
const constants = @import("constants.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(gpa.deinit() == .ok);

    const initial_window_size: i32 = 600;
    var engine = try Engine(MandelbrotSetState, Uniforms, user_functions_impl.user_functions).init(gpa.allocator(), MandelbrotSetState{}, Uniforms{
        .width = initial_window_size,
        .height = initial_window_size,
        .translation_x = constants.initial_mandelbrot_set_translation_x,
        .translation_y = constants.initial_mandelbrot_set_translation_y,
        .scale = constants.initial_mandelbrot_set_scale,
    }, initial_window_size, 144);
    defer engine.deinit();

    try engine.loop();
}
