const std = @import("std");
const Uniforms = @import("engine/vulkan/uniforms.zig").Uniforms;

camera_position: [3]f32 = [3]f32{ 0, 0, 2 },
camera_pitch: f32 = 0,
camera_roll: f32 = 0,
