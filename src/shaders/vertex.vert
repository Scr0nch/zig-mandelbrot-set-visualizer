#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec2 output_texture_coordinate;

void main() {
    vec2[6] vertex_positions = vec2[](
        vec2(0, 0),
        vec2(0, 1),
        vec2(1, 0),
        vec2(1, 0),
        vec2(0, 1),
        vec2(1, 1)
    );
    gl_Position = vec4(vertex_positions[gl_VertexIndex], 0.0, 1.0);
    output_texture_coordinate = vertex_positions[gl_VertexIndex];
}
