const std = @import("std");
const c = @import("c.zig").c;
const constants = @import("constants.zig");
const UserFunctions = @import("engine/user_functions.zig").UserFunctions;
const Engine = @import("engine/engine.zig").Engine;
const MandelbrotSetState = @import("mandelbrot_set_state.zig");
const Uniforms = @import("uniforms.zig").Uniforms;
const input = @import("engine/input.zig");

pub const user_functions = UserFunctions(Uniforms){
    .update = update,
};

fn update(window: *c.GLFWwindow, uniforms: *Uniforms, time_delta: u64) void {
    const delta_factor: f32 = @as(f32, @floatFromInt(time_delta)) / 100.0;
    if (input.isKeyPressed(window, c.GLFW_KEY_W)) {
        uniforms.translation_y -= 0.1 * uniforms.scale * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_S)) {
        uniforms.translation_y += 0.1 * uniforms.scale * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_A)) {
        uniforms.translation_x -= 0.1 * uniforms.scale * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_D)) {
        uniforms.translation_x += 0.1 * uniforms.scale * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_SPACE)) {
        uniforms.scale *= 1.0003 + 0.1 * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_LEFT_SHIFT)) {
        uniforms.scale /= 1.0003 + 0.1 * delta_factor;
    }
    if (input.isKeyPressed(window, c.GLFW_KEY_R)) {
        uniforms.translation_x = constants.initial_mandelbrot_set_translation_x;
        uniforms.translation_y = constants.initial_mandelbrot_set_translation_y;
        uniforms.scale = constants.initial_mandelbrot_set_scale;
    }
}
